#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    (*ui).pbCoffee->setEnabled(false);
    (*ui).pbTea->setEnabled(false);
    (*ui).pbMilk->setEnabled(false);
    (*ui).pbReset->setEnabled(false);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::ChangeMoney(int diff){
    money+=diff;
    (*ui).lcdNumber->display(money);
    Enabled(money);
}

void Widget::Enabled(int diff){
    if(diff >= 100){
        (*ui).pbCoffee->setEnabled(true);
    }
    else{
        (*ui).pbCoffee->setEnabled(false);
    }
    if(diff >= 150){
        (*ui).pbTea->setEnabled(true);
    }
    else{
        (*ui).pbTea->setEnabled(false);
    }
    if(diff >= 200){
        (*ui).pbMilk->setEnabled(true);
    }
    else{
        (*ui).pbMilk->setEnabled(false);
    }
    if(diff >= 10){
        (*ui).pbReset->setEnabled(true);
    }
    else{
        (*ui).pbReset->setEnabled(false);
    }
}

void Widget::on_pb10_clicked()
{
    ChangeMoney(10);
}

void Widget::on_pb50_clicked()
{
    ChangeMoney(50);
}

void Widget::on_pb100_clicked()
{
    ChangeMoney(100);
}

void Widget::on_pb500_clicked()
{
    ChangeMoney(500);
}

void Widget::on_pbCoffee_clicked()
{
    ChangeMoney(-100);
}

void Widget::on_pbTea_clicked()
{
    ChangeMoney(-150);
}

void Widget::on_pbMilk_clicked()
{
    ChangeMoney(-200);
}

void Widget::on_pbReset_clicked()
{
    int R_500 = money / 500;
    int R_100 = (money % 500) / 100;
    int R_50 = ((money % 500) % 100) / 50;
    int R_10 = (((money % 500) % 100) % 50) / 10;
    QString R = QString("500 is %1, 100 is %2, 50 is %3, 10 is %4").arg(R_500).arg(R_100).arg(R_50).arg(R_10);
    QMessageBox m;
    m.information(nullptr, "Return Money!", R);
    money = 0;
    (*ui).lcdNumber->display(money);
}
